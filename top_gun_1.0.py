import pygame
import random
import configparser
import os
pygame.init()

configparser = configparser.RawConfigParser()
configFilePath = os.path.join(os.path.dirname(__file__), 'myconfig.cfg')
configparser.read(configFilePath)
screenheight = configparser.getint("info", "screenheight")
screenwidth = configparser.getint("info", "screenwidth")
pvel = configparser.getint("info", "playervel")
mvel = configparser.getint("info", "missilevel")
clock = pygame.time.Clock()
win = pygame.display.set_mode((screenwidth, screenheight))
pygame.display.set_caption("Top Gun")
bg = pygame.image.load('./pics/bg1.png')
missile_images_l = [pygame.image.load('./pics/1l.png'),
                    pygame.image.load('./pics/2l.png'),
                    pygame.image.load('./pics/3l.png'),
                    pygame.image.load('./pics/4l.png')]
missile_images_r = [pygame.image.load('./pics/1r.png'),
                    pygame.image.load('./pics/2r.png'),
                    pygame.image.load('./pics/3r.png'),
                    pygame.image.load('./pics/4r.png')]
airmine = pygame.image.load('./pics/mine2.png')


class player(object):
    def __init__(self, y, x, icon):
        self.x = x
        self.y = y
        self.height = 64
        self.width = 64
        self.vel = pvel
        self.icon = icon
        self.hitbox = (self.x + 10, self.y + 5, self.x + 50, self.y + 55)

    def move(self, keys, no):
        if no == 0:
            if keys[pygame.K_LEFT] and self.x > self.vel:
                self.x -= self.vel

            if keys[pygame.K_RIGHT] and self.x < 1000 - self.vel - self.width:
                self.x += self.vel

            if keys[pygame.K_UP] and self.y > self.vel:
                self.y -= self.vel
                self.icon = pygame.image.load('./pics/fighter1.png')

            if keys[pygame.K_DOWN] and self.y < 800 - 64 - self.vel - 50:
                self.y += self.vel
                self.icon = pygame.image.load('./pics/fighter2.png')
        elif no == 1:
            if keys[pygame.K_a] and self.x > self.vel:
                self.x -= self.vel

            if (keys[pygame.K_d] and self.x < 1000 - self.vel - self.width):
                self.x += self.vel

            if keys[pygame.K_w] and self.y > self.vel:
                self.y -= self.vel
                self.icon = pygame.image.load('./pics/fighter3.png')

            if keys[pygame.K_s] and self.y < 800 - 64 - self.vel - 50:
                self.y += self.vel
                self.icon = pygame.image.load('./pics/fighter4.png')
        self.hitbox = (self.x + 10, self.y + 5, self.x + 50, self.y + 55)

    def draw(self, win):
        win.blit(self.icon, (self.x, self.y))


class missile(object):

    global lvl

    def __init__(self, x, y):
        self.x = x
        self.y = y
        self.height = 32
        self.width = 32
        self.hitbox = (self.x, self.y + 5, self.x + 35, self.y + 30)
        if x == 0:
            self.icon = missile_images_r[random.choice([0, 1, 2, 3])]
            self.vel = mvel + (lvl * 2)
        else:
            self.icon = missile_images_l[random.choice([0, 1, 2, 3])]
            self.vel = (-1 * mvel) - (lvl * 2)

    def move(self):
        if self.vel < 0 and self.x + self.vel >= 0:
            self.x += self.vel
        elif self.vel > 0 and self.x + self.vel <= screenwidth - self.width:
            self.x += self.vel
        else:
            self.vel = random.choice([mvel + (2 * lvl),
                                      (-1 * mvel) - (2 * lvl)])
            if self.vel > 0:
                self.x = random.randint(-20, 0)
                self.icon = missile_images_r[random.choice([0, 1, 2, 3])]
            else:
                self.x = random.randint(screenwidth, screenwidth + 20)
                self.icon = missile_images_l[random.choice([0, 1, 2, 3])]
        self.hitbox = (self.x, self.y + 5, self.x + 35, self.y + 30)

    def missile_hit(self, p):
        x = False
        if (self.hitbox[0] < p.hitbox[0] and self.hitbox[2] > p.hitbox[0]) or (self.hitbox[0] < p.hitbox[2] and self.hitbox[2] > p.hitbox[2]):
            if (self.hitbox[1] < p.hitbox[1] and self.hitbox[3] > p.hitbox[1]) or (self.hitbox[1] < p.hitbox[3] and self.hitbox[3] > p.hitbox[3]):
                x = True
        return x

    def draw(self, win):
        win.blit(self.icon, (self.x, self.y))


class mine(object):

    def __init__(self, y):
        self.x = random.randint(0, screenwidth - 32)
        self.y = y
        self.hitbox = (self.x + 1, self.y + 1, self.x + 29, self.y + 30)

    def mine_hit(self, p):
        x = False
        if (self.hitbox[0] < p.hitbox[0] and self.hitbox[2] > p.hitbox[0]) or (self.hitbox[0] < p.hitbox[2] and self.hitbox[2] > p.hitbox[2]):
            if (self.hitbox[1] < p.hitbox[1] and self.hitbox[3] > p.hitbox[1]) or (self.hitbox[1] < p.hitbox[3] and self.hitbox[3] > p.hitbox[3]):
                x = True
        return x

    def draw(self, win):
        win.blit(airmine, (self.x, self.y))


def check_hit(p, d):
    global n
    global x
    for i in range(n):
        if launcher[i].missile_hit(p):
            gameover[d] = 1
    for i in range(n):
        for j in range(x):
            if mines[i][j].mine_hit(p):
                gameover[d] = 1
    if (d == 0 and p.y <= 10) or (d == 1 and p.y > 680):
        return False
    else:
        return True


def point_calc(p, d):
    if d == 0:
        for i in range(n):
            if launcher[i].y > p.y:
                points[0] += 10
            if mines[i][0].y > p.y:
                points[0] += 5
    else:
        for i in range(n):
            if launcher[i].y < p.y:
                points[1] += 10
            if mines[i][0].y < p.y:
                points[1] += 5


def print_bar(d):
    global lvl
    over_font = pygame.font.Font('./fonts/spaceage.ttf', 25)
    text1 = over_font.render("Level:", True, (0, 51, 102))
    text2 = over_font.render(str(lvl), 1, (0, 51, 102))
    win.blit(text1, (30, screenheight - 35))
    win.blit(text2, (155, screenheight - 35))
    text1 = over_font.render("Player:", True, (0, 51, 102))
    text2 = over_font.render(str(d+1), 1, (0, 51, 102))
    win.blit(text1, (800 + 30, screenheight - 35))
    win.blit(text2, (880 + 95, screenheight - 35))


def redraw_win(p, d):
    win.blit(bg, (0, 0))
    for i in range(n):
        launcher[i].draw(win)
    for i in range(n):
        for j in range(x):
            mines[i][j].draw(win)
    p.draw(win)
    pygame.draw.rect(win, (0, 51, 102), (0, screenheight - 50, screenwidth, 3))
    print_bar(d)
    pygame.display.update()


def lvlend_pt_print():
    win.blit(bg, (0, 0))
    over_font = pygame.font.Font('./fonts/spaceage.ttf', 40)
    text1 = over_font.render("Points", True, (0, 51, 102))
    win.blit(text1, (screenwidth / 2 - 66, 300))
    text1 = over_font.render("Player1:", True, (0, 51, 102))
    text2 = over_font.render(str(points[0]), 1, (0, 51, 102))
    win.blit(text1, (380, 380))
    win.blit(text2, (620, 380))
    text1 = over_font.render("Player2:", True, (0, 51, 102))
    text2 = over_font.render(str(points[1]), 1, (0, 51, 102))
    win.blit(text1, (370, 450))
    win.blit(text2, (630, 450))
    pygame.display.update()
    pygame.time.delay(1500)


def game_intro_print1():
    win.blit(bg, (0, 0))
    over_font = pygame.font.Font('./fonts/f3.ttf', 80)
    text1 = over_font.render("VVV Studios", True, (0, 51, 102))
    win.blit(text1, (screenwidth / 2 - 280, 360))
    pygame.display.update()
    pygame.time.delay(1000)


def game_intro_print2():
    for i in range(0, 351):
        win.blit(bg, (0, 0))
        win.blit(pygame.image.load("./pics/jet.png"),
                 (-180 + (3 * i), 800 - (3 * i)))
        f = i // 12
        over_font = pygame.font.Font('./fonts/spaceage.ttf', 80)
        text1 = over_font.render("TOP GUN", True, (0, 51, 102))
        win.blit(text1, (screenwidth / 2 - 250, 360))
        pygame.display.update()


def game_over_print():

    win.blit(bg, (0, 0))
    over_font1 = pygame.font.Font('./fonts/spaceage.ttf', 80)
    text1 = over_font1.render("Game Over", True, (0, 51, 102))
    win.blit(text1, (screenwidth / 2 - 300, 350))
    pygame.display.update()
    pygame.time.delay(2000)
    win.blit(bg, (0, 0))
    over_font = pygame.font.Font('./fonts/spaceage.ttf', 80)
    if points[0] > points[1]:
        text1 = over_font.render("Player1 is", True, (0, 51, 102))
    elif points[0] < points[1]:
        text1 = over_font.render("Player2 is", True, (0, 51, 102))
    elif time_points[0] > time_points[1]:
        text1 = over_font.render("Player2 is", True, (0, 51, 102))
    else:
        text1 = over_font.render("Player1 is", True, (0, 51, 102))
    win.blit(text1, (222, 295))
    text1 = over_font.render("Top Gun", True, (0, 51, 102))
    win.blit(text1, (270, 385))
    pygame.display.update()
    pygame.time.delay(2000)


def gameplay(a, b, c, d):
    p = player(a, b, c)
    not_end = True
    while gameover[d] == 0 and not_end:
        clock.tick(40)
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                quit()

        keys = pygame.key.get_pressed()
        for i in range(n):
            launcher[i].move()
        p.move(keys, d)
        not_end = check_hit(p, d)
        redraw_win(p, d)
        time_points[d] += 1
    point_calc(p, d)


def check_repeat():
    x = False
    win.blit(bg, (0, 0))
    over_font = pygame.font.Font('./fonts/spaceage.ttf', 50)
    text1 = over_font.render("press space to play again", True, (0, 51, 102))
    win.blit(text1, (15, 360))
    pygame.display.update()
    for i in range(0, 1001):
        pygame.time.delay(3)
        for event in pygame.event.get():
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_SPACE:
                    x = True
                    return x
    return x


# MAIN

repeat = True
game_intro_print1()
while repeat:
    game_intro_print2()
    lvl = 1
    gameover = [0, 0]
    points = [0, 0]
    time_points = [0, 0]
    x = 4
    while gameover[0] == 0 or gameover[1] == 0:
        launcher = []
        mines = []
        n = 5
        if x <= 12:
            x += 1
        for i in range(n):
            launcher.append(missile(random.choice([0, 1000]), 160 + (120 * i)))
        for i in range(n):
            mines.append([])
            for j in range(x):
                mines[i].append(mine(100 + (120 * i)))
        if gameover[0] != 1:
            gameplay(686, 468, pygame.image.load('./pics/fighter1.png'), 0)
        if gameover[1] != 1:
            gameplay(0, 500 - 32, pygame.image.load('./pics/fighter4.png'), 1)
        lvl += 1
        lvlend_pt_print()
    game_over_print()
    repeat = check_repeat()
pygame.quit()
